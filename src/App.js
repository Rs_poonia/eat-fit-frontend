import React, { Component } from 'react';
import './App.css';
import Header from './components/header/header';
import 'bootstrap/dist/css/bootstrap.min.css';
import ImageSliding from './components/header/imagesliding';
import Catagories from './components/main';
import BottomContainer from './components/bottomcontainer';
import Footer from './components/footer/Footer'
class App extends Component {
  render() {
    return (
      <React.Fragment >
       <Header />
       <ImageSliding />
       <Catagories />
       <BottomContainer/>
       <Footer />
      </React.Fragment>
    );
  }
}

export default App;
