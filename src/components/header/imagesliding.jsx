import React from "react";
import "./header.css";
export default function ImageSliding() {
  return (
    <section>
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-ride="carousel"
      >
        <ol className="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            className="active"
          ></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              className="image-edge-to-edge banner-image fade-in-image image-width image-width-custom"
              src="https://cdn-images.cure.fit/www-curefit-com/image/upload/w_1440,f_auto,ar_2880:595,q_auto:eco/dpr_1.2999999523162842/image/vm/970ac5ff-a30e-448f-b28e-db7319f2050c.jpg"
            />
          </div>
          <div className="carousel-item">
            <img
              className="image-edge-to-edge banner-image fade-in-image image-width image-width-custom"
              src="https://cdn-images.cure.fit/www-curefit-com/image/upload/w_1440,f_auto,ar_2880:595,q_auto:eco/dpr_1.2999999523162842/image/vm/4021928a-0be4-41c2-9853-cf41bfc2b672.jpg"
            />
          </div>
          <div className="carousel-item">
            <img
              className="image-edge-to-edge banner-image fade-in-image image-width image-width-custom"
              src="https://cdn-images.cure.fit/www-curefit-com/image/upload/w_1440,f_auto,ar_2880:595,q_auto:eco/dpr_1.2999999523162842/image/vm/4021928a-0be4-41c2-9853-cf41bfc2b672.jpg"
            />
          </div>
          <div className="carousel-item">
            <img
              className="image-edge-to-edge banner-image fade-in-image image-width image-width-custom"
              src="https://cdn-images.cure.fit/www-curefit-com/image/upload/w_1440,f_auto,ar_2880:595,q_auto:eco/dpr_1.2999999523162842/image/vm/dd52419c-2d6f-476d-8dcf-1422ab3ed9fd.png"
            />
          </div>
        </div>
        <a
          className="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
    </section>
  );
}
