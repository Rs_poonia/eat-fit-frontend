import React, { Component } from 'react'
import './header.css'
export default class Header extends Component {
    render() {
        return (
            <div className="header-tag">
               <div className="header-logo">
               <img src="https://static.cure.fit/assets/images/curefit-v-man.svg"></img>
               <div className="eat-fit-logo">
                <img src="https://cdn-images.cure.fit/www-curefit-com/image/upload/image/header-web/eat-fit.svg"></img>
                <img id="dropdown-arrow" className="dropdown-arrow" src="https://static.cure.fit/assets/images/arrow-clp.svg"></img>
               </div>
               </div>
                <div className="header-middle-section">
                    <a id="id-order">Order</a>
                    <a>Subscribe</a>
                </div>
                <div className="cart-and-login">
                    <div className="login-logo">
                    <img src="https://static.cure.fit/assets/images/user-image.svg"/>
                    <p>Login</p>
                    </div>
                <img src="https://static.cure.fit/assets/images/cart-image.svg"></img>
                </div>
            </div>
        )
    }
}
