import React from 'react'
import './styles.css'
export default function BottomContainer() {
    return (
        <div>
            <div className="container">
            <img className="container-img" src="https://cdn-images.cure.fit/www-curefit-com/image/upload/w_1920,ar_5.2,fl_progressive,f_auto,q_auto/dpr_1.2999999523162842/image/why-eat-fit.png" alt=""/>
            </div>
            
            <div className="typography-text1 ">EAT HEALTHY</div>
            <div className="typography-text2" >STAY FIT</div>
            
        </div>
        
    )
}
