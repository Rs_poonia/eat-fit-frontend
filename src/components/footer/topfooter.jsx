import React from 'react'
import "./styles.css";
export default function TopFooter() {
    return (
        <div>
            <div className="top-footer ">
        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div className="image-description-container">
            <img src="https://static.cure.fit/assets/images/cf-logo.svg" />
            <div className="description">
              At cure.fit, we make group workouts fun, daily food healthy &amp;
              tasty, mental fitness easy with yoga &amp; meditation, and medical
              &amp; lifestyle care hassle-free. #BeBetterEveryDay
            </div>
          </div>
        </div>
        <ul className="static-container col-sm-3 col-xs-12">
          <li>
            <a href="mailto:hello@curefit.com">CONTACT US</a>
          </li>
          <li>
            <a href="/careers">CAREERS</a>
          </li>
          <li>
            <a href="/franchise">CULT FRANCHISE</a>
          </li>
        </ul>
        <div className="play-store-container col-sm-3 ">
          <div className="app-store-image col-xs-6">
            <img src="https://static.cure.fit/assets/images/app-store.svg" />
          </div>
          <div className="play-store-image col-xs-6">
            <img src="https://static.cure.fit/assets/images/play-store.svg" />
          </div>
        </div>
      </div>
        </div>
    )
}
