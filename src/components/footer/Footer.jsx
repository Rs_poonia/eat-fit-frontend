import React from "react";
import TopFooter from './topfooter';
import MiddleFooter from './middlefooter'
import "./styles.css";
export default function Footer() {
  return (
    <div className="footer-container">
      <TopFooter />
      <MiddleFooter />
    </div>
  );
}

// style="padding: 0px 0px 40px;"
