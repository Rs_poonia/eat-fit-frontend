import React from "react";
import './styles.css'
export default function MiddleFooter() {
  return (
    <div className="container middle-footer">
      <div className="middle-footer-content container">
        <div className="col-md-12 p-search" >
          <div className="col-md-12 mid-title">
            <div className="mid-title-text">Popular Searches</div>
          </div>
          <hr className='bottom-line'/>
          <div className="mid-data col-md-12">
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Calorie Counted Diet</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Food Delivery</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Healthy Diet</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Healthy Lunch</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Healthy Meals</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">High Protein Food </a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat/online-breakfast-order">Order Breakfast Online</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat/online-dinner-order">Order Dinner Online</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Order Food Online</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat/online-breakfast-order">Order Healthy Breakfast</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Order Healthy Food Online</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat/online-lunch-order">Order Lunch Online</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat/online-snacks-order">Order Snacks Online</a>
            </div>
            <div className="col-md-2 col-xs-6 col-sm-2">
              <a href="/eat">Weight Loss diet</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
