import React, { Component } from 'react'
import CatagoryNames from './Catagories'
import './styles.css'
import Dish from './dish';
import uuid from "uuid";
const dishes = require('../../../src/csvjson.json')

export default class Catagories extends Component {
    state={
        dishes:[]
    }
    componentDidMount(){
        this.setState({
            dishes:dishes
        })      
    }
    
    render() {
        let disheslocal = this.state.dishes.map(dishEle=>{
            return <Dish className="col-md-4" key={uuid.v4()} dish={dishEle}/>
       
        })
        return (
            <main className="css-1l5li0a  css-1ve8uu8 main-content">
                <CatagoryNames />
                
               <section className="container row css-1l5ok93">
               {disheslocal}
               </section>
            </main>
        )
    }
}
