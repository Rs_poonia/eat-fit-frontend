import React from 'react';
import './styles.css';
import uuid from "uuid";
export default class CatagoryNames extends React.Component {
    state={
        catagories:["Indian Meals","Fit Curries","Fit 98","Drinks","Desserts"]
    }
    render(){
        return (
            <aside>
                {
               this.state.catagories.map(ele=>{
               return <div className="menu-name " key={uuid.v4()}>{ele}</div>
                })}
            </aside>
        )
    }
}
