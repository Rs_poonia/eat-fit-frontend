import React, { Component } from "react";
import "./styles.css";
export default class Dish extends Component {
  render() {
    return (
      <div className="card-wrapper col-xs-12 col-sm-4">
        <div className="css-79elbk">
          <img
            className="border-radius-10 fade-in-image  image-wrapper"
            src={this.props.dish.Image_URL}
          />
          <div className="bottom-half">
            <div className="css-1byjegg clamp-lines ">
              {this.props.dish.Product_Name}
            </div>
            <div className="calorie-info">
              <div className="veg">
                {this.props.dish.Type === "veg" ? (
                  <img src="https://static.cure.fit/assets/images/veg.svg" />
                ) : (
                  <img src="https://static.cure.fit/assets/images/non-veg.svg" />
                )}
              </div>
              <p>
                {this.props.dish.Calories}{" "}
                {this.props.dish.Nutrition_Level
                  ? "| " + this.props.dish.Nutrition_Level
                  : null}
              </p>
            </div>
            <div className="footer-details">
              <div>₹ {this.props.dish.Price}</div>
              <button className="add-cart">ADD</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
